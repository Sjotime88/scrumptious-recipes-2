from http.client import PROXY_AUTHENTICATION_REQUIRED
from mmap import PROT_EXEC
from django.db import models


class Recipe (models.Model): 
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Measure (models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name

class FoodItem (models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name 

class Ingredient (models.Model):
    amount = models.FloatField
    recipe = models.ForeignKey("Recipe", related_name="ingredients", on_delete=models.CASCADE)
    measure = models.ForeignKey("Measure", related_name="ingredient", on_delete=models.PROTECT)
    food = models.ForeignKey("FoodItem", related_name="ingredient",on_delete=models.PROTECT)

    #property = models.ForeignKey(
    #"other model name",
    #related_name="The related_name value creates a property on the other model that allows us to access the instances of Ingredient.
    #on_delete=models.CASCADE,   # (or models.PROTECT)

    def __str__(self):
        return self.name

class Step (models.Model):
    recipe = models.ForeignKey("Recipe", related_name="steps", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", null=True, blank=True)

    def __str__(self):
        return self.name 


    


